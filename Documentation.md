# Facebook  

## Integration Steps

1) **"Install"** or **"Upload"** FGFacebook plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Facebook LTV Threshold Tracking

Please find the documentation for the Facebook LTV Tracker module <a href="fg_facebook_ltv_tracker.html" target="_blank">here</a>.